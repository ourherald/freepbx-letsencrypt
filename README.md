# FreePBX LetsEncrypt automation
The FreePBX GUI does not include a mechanism to use LetsEncrypt's fantastic dns01 challenge and therefore a script must be used to automate that process.

This script makes use of `certbot`, which is available as a snap package in CentOS 7, as per [certbot's official instructions](https://certbot.eff.org/lets-encrypt/centosrhel7-apache).

## Setup instructions
To get this script running: 
- Clone the repository to a directory on your FreePBX box
- Copy with .env.sample file to .env and enter the correct values of the variables for your system
- Install `certbot` and the correct `certbot-dns-` plugin for your provider (see the instructions [here](https://certbot.eff.org/docs/using.html#dns-plugins))
    - Note 1: Sadly, it seems that snapd is the most expedient way to do this on CentOS 7.
    - Note 2: I've only tested this with Cloudflare DNS, so your mileage may vary.
- Run the `get-le-cert.sh` script.
- When it's time to renew the certificate(s), run the `renew-le-certs.sh` script. I like to create a cronjob that does this.

## ToDo
It's unclear whether the script will appropriately install the new certificate or if more work is needed from the `fwconsole sysadmin` command.
