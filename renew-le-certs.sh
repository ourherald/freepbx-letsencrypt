#!/usr/bin/env bash
# This script, intended to be called from cron, 
# calls certbot to renew certs with DNS authentication
#
scriptname=$0
scriptpath=$(dirname "$0")
source $scriptpath/.env

asteriskdir=/etc/asterisk/keys
localdir=$asteriskdir/$targethost
letsencryptdir=/etc/letsencrypt/live/$targethost
logfile=/var/log/acme-certs.log
certname=$(hostname -s)
fwconsole_cmd=/usr/sbin/fwconsole

writelog() {    # Take arguments and write them to logfile (and echo them out)
    echo "${1}" "${2}" "${3}" "${4}"
    echo "$(date)" "${1}"   "${2}" "${3}" "${4}" >> "$logfile"
}


# Do the copying
writelog "Getting certificate from letsencrypt"

certbot renew --dns-${dnsprovider} --dns-${dnsprovider}-credentials "$secretsdir"/${dnsprovider}.ini --preferred-challenges dns-01

# Put certs in a useful place
if [[ ! -d $localdir ]]; then
        rsync -avzZh $letsencryptdir $localdir
fi

# Validity of existing cert
# 2592000 = 4 weeks
# 1209600 = 2 weeks
# 86400   = 1 day
validtimeleft=2592000
validcheck=$(openssl x509 -checkend $validtimeleft -noout -in $asteriskdir/${certname}.crt ; echo $?)
if [[ $validcheck -eq 1 ]]; then
	writelog "Time to replace elderly certificate"
	rm -f $asteriskdir/${certname}.{crt,key}
	
	cp $letsencryptdir/cert.pem $asteriskdir/${certname}.crt
	cp $letsencryptdir/privkey.pem $asteriskdir/${certname}.key
	
	# Import and activate certificate
	writelog "Importing new certificates"
	$fwconsole_cmd certificates --import
	$fwconsole_cmd certificates --default=0
	$fwconsole_cmd sa updatecert
	systemctl restart httpd
else
	writelog "This cert doesn't expire until $(openssl x509 -enddate -noout -in $asteriskdir/${certname}.crt | awk --field-separator = '{print $2}')"
fi

writelog "Cleaning up permissions"
$fwconsole_cmd chown
writelog "Finished"

