#!/usr/bin/env bash
# This script calls certbot to create the initial certs with DNS authentication
#
scriptname=$0
scriptpath=$(dirname "$0")
source "$scriptpath"/.env

asteriskdir=/etc/asterisk/keys
localdir="$asteriskdir"/"$targethost"
letsencryptdir=/etc/letsencrypt/live/"$targethost"
certname="$(hostname -s)"
logfile=/var/log/acme-certs.log

writelog() {    # Take arguments and write them to logfile (and echo them out)
    echo "${1}" "${2}" "${3}" "${4}"
    echo "$(date)" "${1}"   "${2}" "${3}" "${4}" >> "$logfile"
}

## Initial error reporting

if [[ ! -f "$scriptpath"/.env ]]; then
	echo "It looks like you haven't set up your .env file yet. Copy the .env.sample file and tweak the settings to your
	preferences and then try again."
	exit 3
fi

if [[ ! -d "$secretsdir" ]]; then
	echo "It looks like your secrets file isn't set up properly. This file will contain information (such as an API key)
	from your DNS provider so that letsencrypt's request can be processed. Based on your settings in the .env file, add 
	that info to your secrets file here:
	
	$secretsdir/${dnsprovider}.ini
	
	Consult the certbot-dns documentation for guidance: https://certbot.eff.org/docs/using.html#dns-plugins"

	mkdir -p "$secretsdir" && chmod 700 "$secretsdir"
	exit 2
fi

## Make sure snapd is installed
if [[ $(yum list installed | grep -c snapd) -eq 0 ]]; then
	yum install epel-release && \
	yum install snapd && \
	systemctl enable --now snapd.socket || echo "Something went wrong with the snapd setup. You'll have to figure that out before continuing"; exit 4
	ln -s /var/lib/snapd/snap /snap
fi

## Make sure certbot components are installed
if [[ $(snap list | grep -c certbot) -eq 0 ]]; then
	snap install core; snap refresh core
	snap install --classic certbot || echo "Something went wrong installing certbot. Fix that manually and try again"; exit 5
	ln -s /snap/bin/certbot /usr/bin/certbot
fi
if [[ $(snap list | grep -c certbot-dns-$dnsprovider) -eq 0 ]]; then
	snap install certbot-dns-$dnsprovider || echo "Something went wrong installing certbot-dns-$dnsprovider. Fix that manually and try again"; exit 6
fi

## Do the copying
writelog "Getting certificate from letsencrypt"

certbot certonly -d "$targethost" -d "$additionaldomains" --dns-${dnsprovider} --dns-${dnsprovider}-credentials "$secretsdir"/${dnsprovider}.ini --preferred-challenges dns-01

# Put certs in a useful place
if [[ ! -f "$localdir" ]]; then
        ln -s "$letsencryptdir" "$localdir"
fi

# Validity of existing cert
# 1209600 = 2 weeks
# 86400   = 1 day
cp "$localdir"/cert.pem "$asteriskdir"/"${certname}.crt"
cp "$localdir"/privkey.pem "$asteriskdir"/"${certname}.key"

# Import and activate certificate
if [[ -f "$asteriskdir"/"${certname}.key" ]]; then
	writelog "Importing new certificates"
	fwconsole certificate --import
	fwconsole certificate --default=0
	systemctl restart httpd
else
	writelog "Certificate was not properly created"
	exit 1
fi
